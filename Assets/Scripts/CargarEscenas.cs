﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarEscenas : MonoBehaviour
{
    public void LoadScene(string escena)
    {
        SceneManager.LoadScene(escena);
    }

    public void ExitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
}

