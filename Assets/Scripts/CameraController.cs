﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform posicion;
    public float velocidad;
    // Start is called before the first frame update
    void Start()
    {
        posicion = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = posicion.position;
        pos.x += velocidad * Time.deltaTime;
        posicion.position = pos; 
    }
}
